# pixiv isucon

## 初期スコア

`{"pass":true,"score":3268,"success":3067,"fail":0,"messages":[]}`

topで見ていたらmysqlが重いのでそこを重点的に的にみていく


```
mysql> show tables;
+-------------------+
| Tables_in_isuconp |
+-------------------+
| comments          |
| posts             |
| users             |
+-------------------+
3 rows in set (0.00 sec)

mysql> show create table comments;
```


comments, posts のカラムにindex貼ってないので貼る

```sql
ALTER TABLE comments ADD INDEX user_id_idx(user_id);
ALTER TABLE comments ADD INDEX post_id_idx(post_id);

ALTER TABLE posts ADD INDEX user_id_created_at_idx(user_id, created_at);
```


`{"pass":true,"score":6707,"success":6286,"fail":0,"messages":[]}`


appのCPUが高いので静的ファイルをnginx経由で返す

```
upstream app_backend {
    server 127.0.0.1:8000;
}

server {
    listen 8001;

    location \ {
        proxy_pass http://app_backend;
    }

    location ~ \.(css|js|ico)$ {
         root /home/vagrant/pixiv-isucon/webapp/public
         expires 1d;
    }
}
```

画像ファイルのパスがおかしいのでひとまず他の静的ファイルだけ

`{"pass":true,"score":7324,"success":6864,"fail":0,"messages":[]}`

ちょっとだけあがった

DBみたら画像のバイナリデータを保存してそれを都度書いていた｡
なので､ファイルがあったらファイルを返す｡
なかったら作るという方針にする


途中で `client intended to send too large body: 1087213 bytes,` が出たので nginxに`client_max_body_size 100m;` を追加

`{"pass":true,"score":6969,"success":6531,"fail":0,"messages":[]}`

あんまりかわらなかった

読み込んだとき/投稿した時に画像を保存するようにした

```
"静的ファイルが正しくありません (GET /image/9998.jpg)","静的ファイルが正しくありません (GET /image/9999.jpg)
```

とのエラーがでた

ファイルが全部作られてなかったので 全ファイルに雑に wget してファイル作るシェルを実行

コードにtypoがありそれも修正｡


`{"pass":true,"score":15854,"success":15253,"fail":0,"messages":[]}`

DBにデータがあると重いかなと重い imgdata を drop column するも変わらず


次はDBの負荷なので重そうなところを探す
そのためacces_log を ltsv にして出す

だしたら top ページが重いっぽいのでみていく

countまわりが重そうなのでsliceに出した

`{"pass":true,"score":18499,"success":17915,"fail":0,"messages":[]}`


imageURLをcolumnに入れちゃう

```sql
ALTER TABLE posts ADD `url` varchar(191);
```
